module api

go 1.15

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/jmoiron/sqlx v1.3.4
	github.com/kelseyhightower/envconfig v1.4.0 // indirect
	github.com/labstack/echo/v4 v4.5.0
)
