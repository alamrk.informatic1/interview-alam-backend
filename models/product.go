package models

import (
	"api/db"
	"database/sql"
	"log"
	"net/http"
	"strconv"
)

// swagger:model Product
type Product struct {
	ProductID        uint64                `db:"id" json:"product_id"`
	Name             string                `db:"name" json:"name"`
	Quantity         uint64                `db:"quantity" json:"quantity"`
	Description	     *string               `db:"description" json:"description"`
	Active     		 uint8                 `db:"active" json:"active"`
}

func GetProduct(u *Product, id string) int {
	query := "SELECT id, name, quantity, description, active FROM product WHERE id = " + id
	err := db.Db.Get(u, query)
	log.Println(query)
	if err != nil {
		log.Println(err)
		return http.StatusNotFound
	}
	return http.StatusOK
}

func GetAllProducts(u *[]Product, limit uint64, offset uint64, pagination bool, params map[string]string) (uint64, error) {
	query := "SELECT * FROM product"

	var condition string
	// Combine where clause
	clause := false
	for key, value := range params {
		if clause == false {
			condition += " WHERE"
		} else {
			condition += " AND"
		}
		condition += " products." + key + " = '" + value + "'"
		clause = true
	}

	query += condition

	if limit > 0 {
		query += " LIMIT " + strconv.FormatUint(limit, 10)
	}
	if offset > 0 {
		query += " OFFSET " + strconv.FormatUint(offset, 10)
	}

	// Check pagination
	var total uint64
	if pagination == true {
		countQuery := "SELECT COUNT(id) FROM products" + condition
		var totalStr string
		log.Println(countQuery)
		err := db.Db.Get(&totalStr, countQuery)
		if err != nil {
			log.Println(err)
			return 0, err
		}
		total, _ = strconv.ParseUint(totalStr, 10, 64)
	}

	// Main query
	log.Println(query)
	err := db.Db.Select(u, query)
	if err != nil {
		log.Println(err)
		return 0, err
	}
	if pagination == false {
		total = uint64(len(*u))
	}

	return total, nil
}

func CreateProduct(params map[string]string) int {
	query := "INSERT INTO product("
	var fields = ""
	var values = ""
	i := 0
	for key, value := range params {
		fields += "`" + key + "`"
		values += "'" + value + "'"
		if (len(params) - 1) > i {
			fields += ", "
			values += ", "
		}
		i++
	}
	query += fields + ") VALUES(" + values + ")"
	tx, err := db.Db.Begin()
	if err != nil {
		log.Println(err)
		return http.StatusBadGateway
	}
	_, err = tx.Exec(query)
	tx.Commit()
	if err != nil {
		log.Println(err)
		return http.StatusBadRequest
	}
	return http.StatusOK
}

func UpdateProduct(params map[string]string) int {
	query := "UPDATE product SET "
	// Get params
	i := 0
	for key, value := range params {
		if key != "id" {
			query += key + " = '" + value + "'"
			if (len(params) - 2) > i {
				query += ", "
			}
			i++
		}
	}
	query += " WHERE id = " + params["id"]
	log.Println(query)

	tx, err := db.Db.Begin()
	if err != nil {
		log.Println(err)
		return http.StatusBadGateway
	}
	var ret sql.Result
	ret, err = tx.Exec(query)
	row, _ := ret.RowsAffected()
	if row > 0 {
		tx.Commit()
	} else {
		return http.StatusNotFound
	}
	if err != nil {
		log.Println(err)
		return http.StatusBadRequest
	}
	return http.StatusOK
}

func DeleteProduct(id string) int {
	query := "DELETE FROM product WHERE id = " + id
	tx, err := db.Db.Begin()
	if err != nil {
		log.Println(err)
		return http.StatusBadGateway
	}
	var ret sql.Result
	log.Println(query)
	ret, err = tx.Exec(query)
	row, _ := ret.RowsAffected()
	if row > 0 {
		tx.Commit()
	} else {
		return http.StatusNotFound
	}
	if err != nil {
		log.Println(err)
		return http.StatusBadRequest
	}
	return http.StatusOK
}