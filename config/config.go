package config

const (
	Host            string = "http://localhost:1323"
	MariaDBUser     string = "root"
	MariaDBPassword string = ""
	MariaDBDB       string = "interview"
	MariaDBHost     string = "localhost"
	MariaDBPort     string = "3306"
	LimitQuery      uint64 = 100
	Secret          string = "interviewgolang"
)
