package main

import (
	"api/config"
	"api/controllers"
	"api/db"
	"github.com/golang-jwt/jwt"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type jwtCustomClaims struct {
	Name  string `json:"name"`
	Admin bool   `json:"admin"`
	jwt.StandardClaims
}

func Login(c echo.Context) error {
	username := c.FormValue("username")
	password := c.FormValue("password")

	// Throws unauthorized error
	if username != "admin" || password != "admin" {
		return echo.ErrUnauthorized
	}

	// Set custom claims
	claims := &jwtCustomClaims{
		username,
		true,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 3600).Unix(),
		},
	}

	// Create token with claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// Generate encoded token and send it as response.
	t, err := token.SignedString([]byte(config.Secret))
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, echo.Map{
		"token": t,
	})
}

func accessible(c echo.Context) error {
	return c.String(http.StatusOK, "Accessible")
}

func restricted(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(*jwtCustomClaims)
	name := claims.Name
	return c.String(http.StatusOK, "Welcome "+name+"!")
}

func main() {
	// Initialize main database
	db.Db = db.MariaDBInit()

	e := echo.New()
	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Login route
	e.POST("/login", Login)

	r := e.Group("/api")
	// Configure middleware with the custom claims type
	config := middleware.JWTConfig{
		Claims:     &jwtCustomClaims{},
		SigningKey: []byte(config.Secret),
	}
	r.Use(middleware.JWTWithConfig(config))

	r.GET("/product/:id", controllers.GetProduct).Name = "GetProduct"
	r.GET("/products", controllers.GetAllProducts).Name = "GetAllProducts"
	r.POST("/products", controllers.CreateProduct).Name = "CreateProduct"
	r.PUT("/product/:id", controllers.UpdateProduct).Name = "UpdateProduct"
	r.DELETE("/product/:id", controllers.DeleteProduct).Name = "DeletePlaylist"

	e.Logger.Fatal(e.Start(":1323"))
}
