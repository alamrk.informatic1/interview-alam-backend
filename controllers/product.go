package controllers

import (
	"api/config"
	"api/lib"
	"api/models"
	"github.com/labstack/echo/v4"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
)

// GetProduct function
//
// swagger:operation GET /api/product/{id} CRUD-Products GetProduct
//
// Return product based on product ID
//
// ---
// produces:
// - application/json
// parameters:
// - name: id
//   in: path
//   description: product ID
//   required: true
//   type: integer
//   format: uint64
// responses:
//   '200':
//     description: successful operation
//     schema:
//       "$ref": "#/definitions/Product"
//   404:
//     description: not found
func GetProduct(c echo.Context) error {
	idStr := c.Param("id")
	id, _ := strconv.ParseUint(idStr, 10, 64)
	if id == 0 {
		return echo.NewHTTPError(http.StatusNotFound)
	}
	var product models.Product
	status := models.GetProduct(&product, idStr)
	log.Println(status)
	if status == http.StatusOK {
		return c.JSON(status, product)
	} else {
		return echo.NewHTTPError(status)
	}
}

// GetAllProducts function
//
// swagger:operation GET /api/products CRUD-Products GetAllProducts
//
// Returns all products
//
// ---
// produces:
// - application/json
// parameters:
// - name: limit
//   in: query
//   description: maximum number of results to return
//   required: false
//   type: integer
//   format: uint64
// - name: page
//   in: query
//   description: show in page
//   required: false
//   type: integer
//   format: uint64
// - name: pagination
//   in: query
//   description: show in page
//   required: false
//   type: boolean
// responses:
//   '200':
//     description: successful operation
//     schema:
//       "$ref": "#/definitions/Pagination"
//   404:
//     description: not found
func GetAllProducts(c echo.Context) error {
	var err error
	// Get parameter limit
	limitStr := c.QueryParam("limit")
	var limit uint64
	if limitStr != "" {
		limit, err = strconv.ParseUint(limitStr, 10, 64)
		if err == nil {
			if (limit == 0) || (limit > config.LimitQuery) {
				limit = config.LimitQuery
			}
		} else {
			return echo.NewHTTPError(http.StatusBadRequest)
		}
	} else {
		limit = config.LimitQuery
	}
	// Get parameter page
	pageStr := c.QueryParam("page")
	var page uint64
	if pageStr != "" {
		page, err = strconv.ParseUint(pageStr, 10, 64)
		if err == nil {
			if page == 0 {
				page = 1
			}
		} else {
			return echo.NewHTTPError(http.StatusBadRequest)
		}
	} else {
		page = 1
	}
	var offset uint64
	if page > 1 {
		offset = limit * (page - 1)
	}
	// Get parameter pagination
	pagination := false
	paginationStr := c.QueryParam("pagination")
	if paginationStr != "" {
		pagination, err = strconv.ParseBool(paginationStr)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest)
		}
	}

	params := make(map[string]string)
	//Get parameter active
	activeStr := c.QueryParam("active")
	if activeStr != "" {
		var active bool
		active, err = strconv.ParseBool(activeStr)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest)
		}
		if active == true {
			params["active"] = "1"
		} else {
			params["active"] = "0"
		}
	}

	var products []models.Product
	total, err := models.GetAllProducts(&products, limit, offset, pagination, params)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError)
	}
	if total == 0 {
		return echo.NewHTTPError(http.StatusNotFound)
	}
	result := lib.Paginate(c, products, total, limit, page, offset, pagination)
	return c.JSON(http.StatusOK, result)

}

// CreateProduct function
//
// swagger:operation POST /api/product CRUD-Products CreateProduct
//
// Insert new product
//
// ---
// consumes:
// - multipart/form-data
// produces:
// - application/json
// parameters:
// - name: name
//   in: formData
//   description: product name
//   required: true
//   type: string
// - name: quantity
//   in: formData
//   description: quantity
//   required: true
//   type: integer
//   format: uint64
// - name: descripton
//   in: formData
//   description: product descripton
//   required: true
//   type: string
// - name: active
//   in: formData
//   description: if it active product or not
//   required: false
//   type: boolean
// - name: file
//   in: formData
//   description: image file
//   required: true
//   type: file
// responses:
//   '200':
//     description: successful operation
//   '400':
//     description: bad request
//   '502':
//     description: bad gateway
func CreateProduct(c echo.Context) error {
	params := make(map[string]string)

	// Get parameter name
	name := c.FormValue("name")
	if name != "" {
		params["name"] = name
	} else {
		return echo.NewHTTPError(http.StatusBadRequest)
	}

	// Get parameter order
	quantityStr := c.FormValue("quantity")
	if quantityStr != "" {
		order, _ := strconv.ParseUint(quantityStr, 10, 64)
		if order > 0 {
			params["quantity"] = quantityStr
		} else {
			return echo.NewHTTPError(http.StatusBadRequest)
		}
	} else {
		return echo.NewHTTPError(http.StatusBadRequest)
	}

	// Get parameter description
	description := c.FormValue("description")
	if description != "" {
		params["description"] = description
	}

	// Get parameter active
	activeStr := c.FormValue("active")
	if activeStr != "" {
		active, err := strconv.ParseBool(activeStr)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest)
		}
		if active == true {
			params["active"] = "1"
		} else {
			params["active"] = "0"
		}
	} else {
		return echo.NewHTTPError(http.StatusBadRequest)
	}

	// Upload
	file, err := c.FormFile("file")
	if err != nil {
		return err
	}
	src, err := file.Open()
	if err != nil {
		return err
	}
	defer src.Close()

	// Destination
	dst, err := os.Create(file.Filename)
	if err != nil {
		return err
	}
	defer dst.Close()

	// Copy
	if _, err = io.Copy(dst, src); err != nil {
		return err
	}

	status := models.CreateProduct(params)
	return echo.NewHTTPError(status)
}

// UpdateProduct function
//
// swagger:operation PUT /api/product/{id} CRUD-Products UpdateProduct
//
// Update product data
//
// ---
// consumes:
// - multipart/form-data
// produces:
// - application/json
// parameters:
// - name: id
//   in: path
//   description: product ID
//   required: true
//   type: integer
//   format: uint64
// - name: name
//   in: formData
//   description: product name
//   required: true
//   type: string
// - name: quantity
//   in: formData
//   description: quantity
//   required: true
//   type: integer
//   format: uint64
// - name: descripton
//   in: formData
//   description: product descripton
//   required: true
//   type: string
// - name: active
//   in: formData
//   description: if it active product or not
//   required: false
//   type: boolean
// - name: file
//   in: formData
//   description: image file
//   required: true
//   type: file
// responses:
//   '200':
//     description: successful operation
//   '400':
//     description: bad request
//   '502':
//     description: bad gateway	params := make(map[string]string)
func UpdateProduct(c echo.Context) error {
	params := make(map[string]string)

	//Get parameter id
	idStr := c.Param("id")
	id, _ := strconv.ParseUint(idStr, 10, 64)
	if id > 0 {
		params["id"] = strconv.FormatUint(id, 10)
	} else {
		return echo.NewHTTPError(http.StatusBadRequest)
	}

	// Get parameter name
	name := c.FormValue("name")
	if name != "" {
		params["name"] = name
	} else {
		return echo.NewHTTPError(http.StatusBadRequest)
	}

	// Get parameter order
	quantityStr := c.FormValue("quantity")
	if quantityStr != "" {
		order, _ := strconv.ParseUint(quantityStr, 10, 64)
		if order > 0 {
			params["quantity"] = quantityStr
		} else {
			return echo.NewHTTPError(http.StatusBadRequest)
		}
	} else {
		return echo.NewHTTPError(http.StatusBadRequest)
	}

	// Get parameter description
	description := c.FormValue("description")
	if description != "" {
		params["description"] = description
	}

	// Get parameter active
	activeStr := c.FormValue("active")
	if activeStr != "" {
		active, err := strconv.ParseBool(activeStr)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest)
		}
		if active == true {
			params["active"] = "1"
		} else {
			params["active"] = "0"
		}
	} else {
		return echo.NewHTTPError(http.StatusBadRequest)
	}

	// Upload
	file, err := c.FormFile("file")
	if err != nil {
		return err
	}
	src, err := file.Open()
	if err != nil {
		return err
	}
	defer src.Close()

	// Destination
	dst, err := os.Create(file.Filename)
	if err != nil {
		return err
	}
	defer dst.Close()

	// Copy
	if _, err = io.Copy(dst, src); err != nil {
		return err
	}

	status := models.UpdateProduct(params)
	return echo.NewHTTPError(status)
}

func DeleteProduct(c echo.Context) error {
	idrStr := c.Param("id")
	id, _ := strconv.ParseUint(idrStr, 10, 64)
	if id == 0 {
		return echo.NewHTTPError(http.StatusBadRequest)
	}
	status := models.DeleteProduct(strconv.FormatUint(id, 10))
	return echo.NewHTTPError(status)
}